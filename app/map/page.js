"use client";
import Script from "next/script";
import * as React from 'react';

async function getCoordinates(){
    const data = fetch("https://maps.googleapis.com/maps/api/geocode/json?address=929leeavesanleandroca&key=AIzaSyDpoL4EfSObNpf8tMJv3mHI2b24aU2pooc").then(x => x.json())
    return data
}
///
export default function Map(){
    const [coordsData, setCoordData] = React.useState( React.use(getCoordinates()))
    const [coords, setCoords] = React.useState(coordsData.results[0].geometry.location)
    console.log(coords)


    const main = async() => {
        await setupMapKitJs();

        const bayArea = new mapkit.CoordinateRegion(
                new mapkit.Coordinate(coords.lat - 0.3, coords.lng),
                new mapkit.CoordinateSpan(0.65, 0.65)
                );

        const map = new mapkit.Map("map-container");
        map.mapType = mapkit.Map.MapTypes.Satellite
        map.region = bayArea;

        const property = new mapkit.Coordinate(coords.lat, coords.lng);
        const propertyAnnotation = new mapkit.MarkerAnnotation(property);
        propertyAnnotation.color = "#969696";
        propertyAnnotation.selected = "true";
        propertyAnnotation.glyphText = "🛩️";

        map.addItems([ propertyAnnotation]);
    };

    const setupMapKitJs = async() => {
        if (!window.mapkit || window.mapkit.loadedLibraries.length === 0) {
            await new Promise(resolve => { window.initMapKit = resolve });
            delete window.initMapKit;
        }
    const jwt = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IllIWlgzNjlHN0gifQ.eyJpc3MiOiJRUzhTM01LVTZMIiwiaWF0IjoxNjY3OTcwNTU2LCJleHAiOjE2NzA1NjI1MDR9.86HtzzR6G-Cb4mluBQ9YkBrIBlOMCpZA_zNWGR_en_shRinfy8DDyCgGOwHmpXQU_qr1wTDIgwFRqA5NpSub3Q";
        mapkit.init({
            authorizationCallback: done => { done(jwt); }
        });
    };
    return(
            <>
            <Script src="https://cdn.apple-mapkit.com/mk/5.x.x/mapkit.core.js" crossorigin async data-callback="initMapKit" beforeInteractive={true} data-libraries="map,annotations,services" data-initial-token="" onReady={()=> {main();}}></Script>
                <div id="map-container" className={"map w-full h-screen" }/>
           {/* SM, MD SLATE BACKGROUND VERTICAL */}
            <div className={" p-3 fixed z-10 inset-y-0 my-auto h-85 inset-x-0 mx-auto w-120 lg:hidden landscape:hidden"}>
                            <div className={"h-24"}/>
                        <div className={"bg-slate-400 opacity-50 fixed rounded-xl h-135 text-center inset-x-0 mx-auto w-98 md:w-120"}/>
            </div>


            {/* SM, MD LANDSCAPE SLATE BACKGROUND */}
            {/* 50% bottom right position div */}
            <div className = {"hidden sm:landscape:block md:landscape:block lg:landscape:hidden lg:hidden z-10  w-1/2 h-1/2 fixed bottom-0 right-0 z-10 "}>
                <div className={"  bg-slate-400 w-85 h-85 opacity-50 rounded-xl  mx-auto -translate-y-1/2"}>
            </div>
            </div>

            {/* LG, XL, 2XL SLATE BACKGROUND */}
            {/* 50% bottom right position div */}
            <div className = {"hidden z-10 lg:block w-1/2 h-1/2 fixed bottom-0 right-0 z-10"}>
                <div className={"h-97 w-97 bg-slate-400 -translate-y-1/2 mx-auto rounded-xl opacity-50 xl:w-100 xl:h-100"}/>
            </div>


            </>

    )
}



