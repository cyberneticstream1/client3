export default function Content({children}){
    return(
            <>

            {/* SM ACTION MENU */}
            <div className={"leading-tight font-sans text-white text-center p-3 fixed z-50 text-6xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-98 md:hidden mx-auto landscape:hidden"}>
                <div className={"h-24"}></div>
                <div className={"h-8"}/>
                {children}
                <h1>sm</h1>

            </div>


            {/* MD ACTION MENU */}
            <div className={"leading-tight font-sans text-white text-center p-3 fixed z-50 text-8xl inset-y-0 my-auto h-96 inset-x-0 mx-auto w-120 hidden md:block lg:hidden mx-auto landscape:hidden"}>
                <div className={"h-24"}></div>
                <div className={"h-8"}/>

                {children}
                <h1>md</h1>

            </div>

            {/* XL ACTION MENU */}
            <div className = {" leading-tight font-sans text-white text-center text-7xl hidden xl:block w-1/2 h-1/2 fixed bottom-0 right-0 z-50"}>
                <div className={"h-96 w-96 bg -translate-y-1/2 mx-auto rounded-xl xl:w-96 xl:h-96"}>

                    {children}
                    <h1>xl</h1>

                </div>
            </div>

            {/* LG ACTION MENU */}
            <div className = {" leading-tight font-sans text-white text-center text-6xl hidden lg:block xl:hidden w-1/2 h-1/2 fixed bottom-0 right-0 z-50"}>
                <div className={"h-96 w-96 bg -translate-y-1/2 mx-auto rounded-xl xl:w-96 xl:h-96"}>
                    <div className={"h-4 opacity-50"}></div>

                    {children}
                    <h1>lg</h1>
                </div>
            </div>

            {/* MD, LANDSCAPE ACTION MENU */}



            </>

    )
}
